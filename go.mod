module go.elara.ws/infinitime

go 1.16

require (
	github.com/fxamacker/cbor/v2 v2.4.0
	github.com/godbus/dbus/v5 v5.0.6
	github.com/muka/go-bluetooth v0.0.0-20220819140550-1d8857e3b268
	go.elara.ws/logger v0.0.0-20230928062203-85e135cf02ae
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
)
