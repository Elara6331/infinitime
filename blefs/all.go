package blefs

import (
	"path/filepath"
	"strings"
)

func (blefs *FS) RemoveAll(path string) error {
	if path == "" {
		return nil
	}

	if filepath.Clean(path) == "/" {
		return ErrNoRemoveRoot
	}

	fi, err := blefs.Stat(path)
	if err != nil {
		return nil
	}

	if fi.IsDir() {
		return blefs.removeAllChildren(path)
	} else {
		err = blefs.Remove(path)

		var code int8
		if err, ok := err.(FSError); ok {
			code = err.Code
		}

		if err != nil && code != -2 {
			return err
		}
	}

	return nil
}

func (blefs *FS) removeAllChildren(path string) error {
	list, err := blefs.ReadDir(path)
	if err != nil {
		return err
	}

	for _, entry := range list {
		name := entry.Name()

		if name == "." || name == ".." {
			continue
		}
		entryPath := filepath.Join(path, name)

		if entry.IsDir() {
			err = blefs.removeAllChildren(entryPath)
		} else {
			err = blefs.Remove(entryPath)
		}

		var code int8
		if err, ok := err.(FSError); ok {
			code = err.Code
		}

		if err != nil && code != -2 {
			return err
		}
	}

	return nil
}

func (blefs *FS) MkdirAll(path string) error {
	if path == "" || path == "/" {
		return nil
	}

	splitPath := strings.Split(path, "/")
	for i := 1; i < len(splitPath); i++ {
		curPath := strings.Join(splitPath[0:i+1], "/")

		err := blefs.Mkdir(curPath)

		var code int8
		if err, ok := err.(FSError); ok {
			code = err.Code
		}

		if err != nil && code != -17 {
			return err
		}
	}

	return nil
}
