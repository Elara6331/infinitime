# InfiniTime

> **Warning** This library is no longer maintained. A rewrite has been merged into the ITD repo in [the infinitime subpackage](https://gitea.elara.ws/Elara6331/itd/src/branch/master/infinitime)

This is a go library for interfacing with InfiniTime firmware
over BLE on Linux.

[![Go Reference](https://pkg.go.dev/badge/go.elara.ws/infinitime.svg)](https://pkg.go.dev/go.elara.ws/infinitime)

---

### Importing

This library's import path is `go.elara.ws/infinitime`.

---

### Dependencies

This library requires `dbus`, and `bluez` to function. These allow the library to use bluetooth, control media, control volume, etc.

#### Arch

```shell
sudo pacman -S dbus bluez --needed
```

#### Debian/Ubuntu

```shell
sudo apt install dbus bluez
```

#### Fedora

```shell
sudo dnf install dbus bluez
```

---

### Features

This library currently supports the following features:

- Notifications
- Heart rate monitoring
- Setting time
- Battery level
- Music control
- OTA firmware upgrades
- Navigation

---

### Mentions

The DFU process used in this library was created with the help of [siglo](https://github.com/alexr4535/siglo)'s source code. Specifically, this file: [ble_dfu.py](https://github.com/alexr4535/siglo/blob/main/src/ble_dfu.py)
